/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.common.constant;

public class TransactionConstant {

    /**
     * Transaction environment context parameter name (attached to user remote method call)
     */
    public final static String TRANSACTION_CONTEXT = "transactionContext";

    /**
     * Distributed transaction log table prefix
     */
    public final static String TRANSACTION_LOG_PREFIX = "transaction_log";

    public final static Integer JDBC_ERROR = 0;

    public final static String TRANSACTION_CONTEXT_HTTP_HEADER = "TRANSACTION_CONTEXT_HTTP_HEADER";

    /**
     * Path of extension point load
     */
    public static final String EXTENSION_LOAD_PATH = "META-INF/transaction/";

    public static final String TRANSACTION_REPOSITORY_JDBC = "jdbcrepository";

    public static final String TRANSACTION_REPOSITORY_REDIS = "redisrepository";

    public static final String TRANSACTION_MESSAGE_ACTIVEMQ = "activemqmessage";

    public static final String TRANSACTION_MESSAGE_KAFKA = "kafkamessage";

    public static final String MQ_TOPIC_GOODS = "goods";

    public static final String MQ_TOPIC_MEMBER = "member";

    /**
     * Hessian serialization
     */
    public static final String SERIALIZE_HESSIAN = "hessian";
    /**
     * Hessian2 serialization
     */
    public static final String SERIALIZE_HESSIAN2 = "hessian2";

    /**
     * Java serialization
     */
    public static final String SERIALIZE_JAVA = "java";
    /**
     * protobuf serialization
     */
    public static final String SERIALIZE_PROTOBUF = "protobuf";
    /**
     * json serialization
     */
    public static final String SERIALIZE_JSON = "json";

    /**
     * Number of transaction log message event caches
     */
    public static final Integer EVENT_BUFFER_SIZE = 2048;

    /**
     * Delay time (in seconds) for retry call of faulted transaction
     */
    public static final Integer RETRIED_DELAY_TIME = 60;

    /**
     * The cycle time (in seconds) that the failed transaction retries the call
     */
    public static final Integer RETRIED_PERIOD = 120;

    /**
     * Maximum number of retry calls for an error transaction
     */
    public static final Integer RETRIED_COUNT = 4;
}
