/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.common.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * Transaction participant
 */
@Data
public class Participant implements Serializable {

    private static final long serialVersionUID = 1L;
    //Transaction ID
    private String transactionId;

    //Message delivery model used to send messages (e.g. peer-to-peer, publish / subscribe)
    private Integer messageModel;

    //Where to send reliable messages
    private String destination;

    //Transaction participant related classes
    private Class targetClass;

    //The method name called by the transaction participant
    private String methodName;

    //The type of method parameter invoked by the transaction participant
    private Class[] parameterTypes;

    //The parameter value of the method called by the transaction participant
    private Object[] arguments;
}
