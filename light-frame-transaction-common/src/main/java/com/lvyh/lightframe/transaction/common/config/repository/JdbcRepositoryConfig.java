/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.common.config.repository;

import lombok.Data;

/**
 * JDBC storage configuration
 */
@Data
public class JdbcRepositoryConfig extends BaseRepositoryConfig {
    private String driverClassName = "com.mysql.cj.jdbc.Driver";

    private String url;

    private String username;

    private String password;

    private int initialSize = 10;

    private int maxActive = 100;

    private int minIdle = 20;

    private int maxWait = 60000;

    private int timeBetweenEvictionRunsMillis = 60000;

    private int minEvictableIdleTimeMillis = 300000;

    private String validationQuery = " SELECT 1 ";

    private Boolean testOnBorrow = false;

    private Boolean testOnReturn = false;

    private Boolean testWhileIdle = true;

    private Boolean poolPreparedStatements = false;

    private int maxPoolPreparedStatementPerConnectionSize = 100;
}
