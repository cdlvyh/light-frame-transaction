/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.common.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Transaction object
 */
@Data
public class Transaction implements Serializable {
    private static final long serialVersionUID = 1L;

    //Transaction ID
    private String transactionId;

    //Transaction creation time
    private Date createTime = new Date();

    //Last modification time
    private Date lastTime = new Date();

    //Transaction role
    private Integer role;

    //Transaction status
    private Integer status;

    //The fully qualified name of the calling class
    private String targetClass;

    //Method name called
    private String targetMethod;

    //error message
    private String errorMessage;

    //retry count
    private Integer retriedCount = 0;

    //Number of MQ messages sent
    private Integer sendMessageCount = 0;

    //Transaction participant collection
    private List<Participant> participants = new CopyOnWriteArrayList();
}
