/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.dubbo.interceptor;

import com.alibaba.dubbo.rpc.RpcContext;
import com.alibaba.fastjson.JSON;
import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.common.context.TransactionContext;
import com.lvyh.lightframe.transaction.core.coordinator.RemoteTransactionCoordinator;
import com.lvyh.lightframe.transaction.core.interceptor.TransactionInterceptor;
import com.lvyh.lightframe.transaction.core.service.RemoteCoordinatorService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;


/**
 * Dubbo method pointcut interception
 */
@Slf4j
public class DubboTransactionInterceptor implements TransactionInterceptor {

    //Provides a handler for transaction
    private RemoteTransactionCoordinator remoteTransactionCoordinator;

    @Autowired
    private RemoteCoordinatorService remoteCoordinatorService;

    /**
     * Pointcut interception processing
     */
    @Override
    public Object interceptor(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        //Get the parameter value passed by Dubbo, which is transmitted remotely, and the data format is JSON
        String remoteTransactionContextValue = RpcContext.getContext().getAttachment(TransactionConstant.TRANSACTION_CONTEXT);

        //Remote incoming transaction environment context object
        TransactionContext remoteTransactionContext = null;

        //If transaction context value is not null, it is converted to TransactionContext object.
        if (!StringUtils.isEmpty(remoteTransactionContextValue)) {
            remoteTransactionContext = JSON.parseObject(remoteTransactionContextValue, TransactionContext.class);
        }

        //Gets the specific handler policy of the transaction
        remoteTransactionCoordinator = remoteCoordinatorService.getHandlerStrategy(remoteTransactionContext);

        log.info("Enter the AOP method and obtain the RPC parameters transmitted from the remote as follows:" + remoteTransactionContextValue + ", the current local transaction role is:" + remoteTransactionCoordinator.handlerRoleType().getDesc());
        Object proceed = remoteTransactionCoordinator.handler(proceedingJoinPoint, remoteTransactionContext);
        return proceed;
    }
}
