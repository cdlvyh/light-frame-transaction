/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.interceptor;

import com.alibaba.fastjson.JSON;
import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.common.context.TransactionContext;
import com.lvyh.lightframe.transaction.core.context.SimpleRpcContext;
import com.lvyh.lightframe.transaction.core.coordinator.RemoteTransactionCoordinator;
import com.lvyh.lightframe.transaction.core.interceptor.TransactionInterceptor;
import com.lvyh.lightframe.transaction.core.service.RemoteCoordinatorService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Springcloud method pointcut interception
 */
@Slf4j
public class SpringcloudTransactionInterceptor implements TransactionInterceptor {

    //Provides a handler for transaction
    private RemoteTransactionCoordinator remoteTransactionCoordinator;

    @Autowired
    private RemoteCoordinatorService remoteCoordinatorService;

    /**
     * Pointcut interception processing
     */
    @Override
    public Object interceptor(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        if (attributes == null) {
            log.info("The data compensation operation directly jumps out when it is called by reflection.");
            return proceedingJoinPoint.proceed();
        }

        RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        String transactionContextValue = request.getHeader(TransactionConstant.TRANSACTION_CONTEXT_HTTP_HEADER);

        String remoteTransactionContextValue = SimpleRpcContext.getContext().getAttachment(TransactionConstant.TRANSACTION_CONTEXT);

        //Remote incoming transaction environment context object
        TransactionContext remoteTransactionContext = null;

        //If transaction context value is not null, it is converted to TransactionContext object.
        if (!StringUtils.isEmpty(transactionContextValue)) {
            remoteTransactionContext = JSON.parseObject(remoteTransactionContextValue, TransactionContext.class);
        }

        //Gets the specific handler policy of the transaction
        remoteTransactionCoordinator = remoteCoordinatorService.getHandlerStrategy(remoteTransactionContext);

        log.info("Enter the AOP method and obtain the RPC parameters transmitted from the remote as follows:" + remoteTransactionContextValue + ", the current local transaction role is:" + remoteTransactionCoordinator.handlerRoleType().getDesc());
        Object proceed = remoteTransactionCoordinator.handler(proceedingJoinPoint, remoteTransactionContext);
        return proceed;
    }
}
