/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.feignclient;

import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.core.context.SimpleRpcContext;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLDecoder;

/**
 * @author lvyh 2021/11/18.
 */
public class TransactionContextClientInterceptor implements RequestInterceptor {

    private static final Logger log = LoggerFactory.getLogger(TransactionContextClientInterceptor.class);

    @Override
    public void apply(RequestTemplate requestTemplate) {
        //Take out the transaction context information and put it into feign's request header
        String transactionContextValue = SimpleRpcContext.getContext().getAttachment(TransactionConstant.TRANSACTION_CONTEXT);
        if (transactionContextValue != null) {
            try {
                log.info("[TransactionContextClientInterceptor] print transaction context value:{}", transactionContextValue);
                requestTemplate.header(TransactionConstant.TRANSACTION_CONTEXT_HTTP_HEADER, new String[]{URLDecoder.decode(transactionContextValue, "UTF-8")});
            } catch (Exception e) {
                log.error("[TransactionContextClientInterceptor] transaction context information setting error,", e);
            }
        }
    }
}

