/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.feignclient;

import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.core.context.SimpleRpcContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author lvyh 2021/11/18.
 */
public class TransactionContextFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(TransactionContextClientInterceptor.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        this.initTransactionContext((HttpServletRequest) request);
        chain.doFilter(request, response);
    }

    private void initTransactionContext(HttpServletRequest request) {
        String transactionContextValue = request.getHeader(TransactionConstant.TRANSACTION_CONTEXT_HTTP_HEADER);
        if (StringUtils.isNotBlank(transactionContextValue)) {
            try {
                SimpleRpcContext.getContext().setAttachment(TransactionConstant.TRANSACTION_CONTEXT, transactionContextValue);
                log.info("[TransactionContextFilter] print transaction context：{}", transactionContextValue);
            } catch (Exception e) {
                log.error("TransactionContextFilter init transaction context error", e);
            }
        }
    }

    @Override
    public void destroy() {
    }
}
