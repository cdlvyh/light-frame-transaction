# 快速开始 

在本文档中(仅提供Dubbo使用案例，Springcloud案例类似)，将创建若干 Spring Boot 工程（详见light-frame-transaction-dubbo-sample父模块），并引入 light-frame-transaction 框架的客户端组件，然后演示如何快速上手 light-frame-transaction 。

## 一、环境准备 

light-frame-transaction 依赖以下Java环境：JDK8及以上，需要采用 Apache Maven 3.5.0 或者以上的版本来编译。

### 1.1、初始化数据库
    
请下载项目源码并解压，获取数据库初始化SQL脚本并执行即可。脚本位置如下：

```
light-frame-transaction\doc\sql\light-frame-transaction-goods.sql、

light-frame-transaction\doc\sql\light-frame-transaction-member.sql、

light-frame-transaction\doc\sql\light-frame-transaction-order.sql
```

### 1.2、启动zk服务

启动zk，可以参考当前试验zk版本及命令如下：

```
$ cd C:\softmy\kafka\kafka_2.12-2.8.0\bin\windows

zookeeper-server-start.bat C:\softmy\kafka\kafka_2.12-2.8.0\config\zookeeper.properties
```

其中，zk配置内容如下：

```
dataDir=/tmp/zookeeper

clientPort=2181

maxClientCnxns=0

admin.enableServer=false

```

### 1.3、启动kafka

启动zk，可以参考当前试验zk版本及命令如下：

```
$ cd C:\softmy\kafka\kafka_2.12-2.8.0\bin\windows

kafka-server-start.bat C:\softmy\kafka\kafka_2.12-2.8.0\config\server.properties
```
其中，server.properties配置内容如下：

```
broker.id=0
num.network.threads=3
num.io.threads=8
socket.send.buffer.bytes=102400
socket.receive.buffer.bytes=102400
socket.request.max.bytes=104857600
log.dirs=C:\\softmy\\kafka\\kafka_2.12-2.8.0\\logs
num.partitions=1
num.recovery.threads.per.data.dir=1
offsets.topic.replication.factor=1
transaction.state.log.replication.factor=1
transaction.state.log.min.isr=1
log.retention.hours=168
log.segment.bytes=1073741824
log.retention.check.interval.ms=300000
zookeeper.connect=localhost:2181
zookeeper.connection.timeout.ms=18000
group.initial.rebalance.delay.ms=0

```

### 1.4、启动consul服务（可选，仅springcloud案例需要）

启动consul，可以参考当前试验consul版本及命令如下：

```
$ cd C:\softmy\consul_1.7.2_windows_amd64

consul agent -dev
```
然后，打开网址：http://localhost:8500 ，可以看到当前注册的服务。


## 二、创建Sample工程

使用IDEA Spring Boot的项目生成工具来生成样例工程，同时添加一个Web依赖，以便在浏览器中查看最终效果。

## 三、引入maven依赖

创建好Spring Boot工程（以 light-frame-transaction-dubbo-sample-order-server 项目举例，其他系统创建操作类似）之后，接下来引入各种依赖。

首先，修改maven项目的配置文件pom.xml，将

```
<parent>
    <artifactId>light-frame-transaction-dubbo-sample</artifactId>
    <groupId>com.lvyh.lightframe</groupId>
    <version>${light.frame.transaction.version}</version>
</parent>
```

这里的 ${light.frame.transaction.version} 指定具体的 light-frame-transaction 版本，参考发布历史。 

然后，添加 light-frame-transaction 客户端组件依赖及Web依赖：

```
<dependency>
    <groupId>com.lvyh.lightframe</groupId>
    <artifactId>light-frame-transaction-dubbo-sample-order-api</artifactId>
</dependency>

<dependency>
    <groupId>com.lvyh.lightframe</groupId>
    <artifactId>light-frame-transaction-dubbo-spring-boot-starter</artifactId>
</dependency>

<dependency>
    <groupId>com.lvyh.lightframe</groupId>
    <artifactId>light-frame-transaction-jdbc-repository-spring-boot-starter</artifactId>
</dependency>
<dependency>
    <groupId>com.lvyh.lightframe</groupId>
    <artifactId>light-frame-transaction-kafka-spring-boot-starter</artifactId>
</dependency>

<dependency>
     <groupId>org.springframework.boot</groupId>
     <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

## 四、运行项目

首先，分别启动以下项目：DubboSampleClientApplication、DubboSampleGoodsApplication、DubboSampleMemberApplication、DubboSampleOrderApplication。

注：以启动 DubboSampleOrderApplication 项目为例，IDEA中直接通过 DubboSampleOrderApplication 的 main 方法启动应用，具体如下：

```
@SpringBootApplication
@ComponentScan(basePackages = {"com.lvyh.lightframe.*"})
public class DubboSampleOrderApplication {
    private static final Logger logger = LoggerFactory.getLogger(DubboSampleOrderApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DubboSampleOrderApplication.class, args);
        logger.info("[DubboSampleOrderApplication] start up complete.");

    }
}
```

然后，在控制台中查看启动打印的日志。

## 五、测试

### 5.1、验证事务正常情况

首先，用户浏览器访问以下地址，手动触发商品下单、减库存、扣款等分布式事务操作。
```
http://localhost:8080/create-order?count=1&price=1

```

然后，查看各微服务系统打印日志，如下：

订单系统:
```
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - [AbstractTransactionAspect] Enter the interceptor!
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - Enter the AOP method and obtain the RPC parameters transmitted from the remote as follows:null, the current local transaction role is:事务发起者
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - [StartTransactionCoordinator] begin transaction!
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - [TransactionCoordinatorSupport] build transaction, clazz:com.lvyh.lightframe.transaction.dubbo.sample.order.api.service.OrderInfoService, method:makePayment, role:1
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - Transaction log insert, transaction:Transaction(transactionId=527738888, createTime=Tue Nov 23 13:48:41 CST 2021, lastTime=Tue Nov 23 13:48:41 CST 2021, role=1, status=1, targetClass=com.lvyh.lightframe.transaction.dubbo.sample.order.api.service.OrderInfoService, targetMethod=makePayment, errorMessage=null, retriedCount=0, sendMessageCount=0, participants=[])
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] DEBUG - ==>  Preparing: select 'true' as QUERYID, id, user_id, product_id, total_amount, buy_count, order_status, create_time, update_time from transaction_order WHERE ( id = ? ) 
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] DEBUG - ==> Parameters: 155(Long)
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] DEBUG - <==      Total: 1
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] DEBUG - ==>  Preparing: update transaction_order SET user_id = ?, product_id = ?, total_amount = ?, buy_count = ?, order_status = ?, create_time = ?, update_time = ? where id = ? 
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] DEBUG - ==> Parameters: 1(Long), 1(Long), 1(Long), 1(Integer), 4(Integer), 2021-11-23 13:48:41.0(Timestamp), 2021-11-23 13:48:41.223(Timestamp), 155(Long)
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] DEBUG - <==    Updates: 1
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - [OrderInfoServiceImpl] update transaction order status, orderId:155
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - 1. Enter Dubbo custom handler and call the following method:
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - 2. Pass the transaction participant context object as an RPC interface parameter to the remote method:{"role":1,"transactionId":"527738888"}
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - Transaction log update , transaction:Transaction(transactionId=527738888, createTime=Tue Nov 23 13:48:41 CST 2021, lastTime=Tue Nov 23 13:48:41 CST 2021, role=1, status=1, targetClass=com.lvyh.lightframe.transaction.dubbo.sample.order.api.service.OrderInfoService, targetMethod=makePayment, errorMessage=null, retriedCount=0, sendMessageCount=0, participants=[Participant(transactionId=527738888, messageModel=2, destination=member, targetClass=interface com.lvyh.lightframe.transaction.dubbo.sample.member.api.service.UserAccountService, methodName=payment, parameterTypes=[class com.lvyh.lightframe.transaction.dubbo.sample.member.api.domain.UserAccountRequest], arguments=[UserAccountRequest(userId=1, totalAmount=1)])])
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - 3. Add transaction participant:interface com.lvyh.lightframe.transaction.dubbo.sample.member.api.service.UserAccountService.payment
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - [OrderInfoServiceImpl] invoke member payment, orderId:155
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - 1. Enter Dubbo custom handler and call the following method:
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - 2. Pass the transaction participant context object as an RPC interface parameter to the remote method:{"role":1,"transactionId":"527738888"}
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - Transaction log update , transaction:Transaction(transactionId=527738888, createTime=Tue Nov 23 13:48:41 CST 2021, lastTime=Tue Nov 23 13:48:41 CST 2021, role=1, status=1, targetClass=com.lvyh.lightframe.transaction.dubbo.sample.order.api.service.OrderInfoService, targetMethod=makePayment, errorMessage=null, retriedCount=0, sendMessageCount=0, participants=[Participant(transactionId=527738888, messageModel=2, destination=member, targetClass=interface com.lvyh.lightframe.transaction.dubbo.sample.member.api.service.UserAccountService, methodName=payment, parameterTypes=[class com.lvyh.lightframe.transaction.dubbo.sample.member.api.domain.UserAccountRequest], arguments=[UserAccountRequest(userId=1, totalAmount=1)]), Participant(transactionId=527738888, messageModel=2, destination=goods, targetClass=interface com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService, methodName=decrease, parameterTypes=[class com.lvyh.lightframe.transaction.dubbo.sample.goods.api.domain.InventoryRequest], arguments=[InventoryRequest(productId=1, count=1)])])
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - 3. Add transaction participant:interface com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService.decrease
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - [OrderInfoServiceImpl] invoke stock decrease, orderId:155
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - Transaction log update , transaction:Transaction(transactionId=527738888, createTime=Tue Nov 23 13:48:41 CST 2021, lastTime=Tue Nov 23 13:48:42 CST 2021, role=1, status=3, targetClass=com.lvyh.lightframe.transaction.dubbo.sample.order.api.service.OrderInfoService, targetMethod=makePayment, errorMessage=null, retriedCount=0, sendMessageCount=0, participants=[Participant(transactionId=527738888, messageModel=2, destination=member, targetClass=interface com.lvyh.lightframe.transaction.dubbo.sample.member.api.service.UserAccountService, methodName=payment, parameterTypes=[class com.lvyh.lightframe.transaction.dubbo.sample.member.api.domain.UserAccountRequest], arguments=[UserAccountRequest(userId=1, totalAmount=1)]), Participant(transactionId=527738888, messageModel=2, destination=goods, targetClass=interface com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService, methodName=decrease, parameterTypes=[class com.lvyh.lightframe.transaction.dubbo.sample.goods.api.domain.InventoryRequest], arguments=[InventoryRequest(productId=1, count=1)])])
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - [StartTransactionCoordinator] commit transaction!
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - [StartTransactionCoordinator] send mq message to transaction participants!
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:54716-thread-3] INFO  - [TransactionCoordinatorSupport] transaction participants:2

```

会员系统:
```
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - [AbstractTransactionAspect] Enter the interceptor!
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - Enter the AOP method and obtain the RPC parameters transmitted from the remote as follows:{"role":1,"transactionId":"527738888"}, the current local transaction role is:事务参与者
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - [TransactionCoordinatorSupport] build transaction, clazz:com.lvyh.lightframe.transaction.dubbo.sample.member.api.service.UserAccountService, method:payment, role:2
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - Transaction log insert, transaction:Transaction(transactionId=527738888, createTime=Tue Nov 23 13:48:41 CST 2021, lastTime=Tue Nov 23 13:48:41 CST 2021, role=2, status=1, targetClass=com.lvyh.lightframe.transaction.dubbo.sample.member.api.service.UserAccountService, targetMethod=payment, errorMessage=null, retriedCount=0, sendMessageCount=0, participants=[])
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - [ActorTransactionCoordinator] participant transaction!
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] DEBUG - ==>  Preparing: select 'true' as QUERYID, id, user_id, balance, create_time, update_time from transaction_user_account WHERE ( user_id = ? ) 
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] DEBUG - ==> Parameters: 1(Long)
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] DEBUG - <==      Total: 1
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] DEBUG - ==>  Preparing: update transaction_user_account SET user_id = ?, balance = ?, create_time = ?, update_time = ? where id = ? 
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] DEBUG - ==> Parameters: 1(Long), 99(Long), 2021-11-08 17:30:01.0(Timestamp), 2021-11-23 13:48:41.966(Timestamp), 1(Long)
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] DEBUG - <==    Updates: 1
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - Transaction log update , transaction:Transaction(transactionId=527738888, createTime=Tue Nov 23 13:48:41 CST 2021, lastTime=Tue Nov 23 13:48:41 CST 2021, role=2, status=3, targetClass=com.lvyh.lightframe.transaction.dubbo.sample.member.api.service.UserAccountService, targetMethod=payment, errorMessage=null, retriedCount=0, sendMessageCount=0, participants=[])
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - [ActorTransactionCoordinator] commit transaction!
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - [ActorTransactionCoordinator] send mq message to transaction participants!
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - [TransactionCoordinatorSupport] transaction participants:
2021-11-23 13:48:41 [DubboServerHandler-172.17.61.191:54597-thread-2] INFO  - [AbstractTransactionAspect] Exit the interceptor!

```

商品系统:
```
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - [AbstractTransactionAspect] Enter the interceptor!
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - Enter the AOP method and obtain the RPC parameters transmitted from the remote as follows:{"role":1,"transactionId":"527738888"}, the current local transaction role is:事务参与者
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - [TransactionCoordinatorSupport] build transaction, clazz:com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService, method:decrease, role:2
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - Transaction log insert, transaction:Transaction(transactionId=527738888, createTime=Tue Nov 23 13:48:42 CST 2021, lastTime=Tue Nov 23 13:48:42 CST 2021, role=2, status=1, targetClass=com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService, targetMethod=decrease, errorMessage=null, retriedCount=0, sendMessageCount=0, participants=[])
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - [ActorTransactionCoordinator] participant transaction!
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] DEBUG - ==>  Preparing: select 'true' as QUERYID, id, product_id, total_inventory, create_time, update_time from transaction_inventory WHERE ( product_id = ? ) 
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] DEBUG - ==> Parameters: 1(Long)
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] DEBUG - <==      Total: 1
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] DEBUG - ==>  Preparing: update transaction_inventory SET product_id = ?, total_inventory = ?, create_time = ?, update_time = ? where id = ? 
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] DEBUG - ==> Parameters: 1(Long), 99(Integer), 2021-11-08 17:30:01.0(Timestamp), 2021-11-23 13:48:42.78(Timestamp), 1(Long)
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] DEBUG - <==    Updates: 1
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - Transaction log update , transaction:Transaction(transactionId=527738888, createTime=Tue Nov 23 13:48:42 CST 2021, lastTime=Tue Nov 23 13:48:42 CST 2021, role=2, status=3, targetClass=com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService, targetMethod=decrease, errorMessage=null, retriedCount=0, sendMessageCount=0, participants=[])
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - [ActorTransactionCoordinator] commit transaction!
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - [ActorTransactionCoordinator] send mq message to transaction participants!
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - [TransactionCoordinatorSupport] transaction participants:
2021-11-23 13:48:42 [DubboServerHandler-172.17.61.191:55000-thread-2] INFO  - [AbstractTransactionAspect] Exit the interceptor!
2021-11-23 13:49:00 [transaction-recover-1-thread-1] INFO  - [TransactionRecoverTask] transaction recover task running.

```

最后，查看各数据表字段状态变化，如下：

transaction_order交易订单表增加了一条订单，transaction_user_account交易用户账号表中的balance用户余额字段扣了1，transaction_inventory交易库存表中的total_inventory总库存字段减了1。 

### 5.2、验证事务异常情况

首先，停掉商品系统，模拟系统宕机，再次访问
```
http://localhost:8080/create-order?count=1&price=1

```

然后，同上观察应用日志以及数据表字段状态变化，可见订单生成了，用户余额也扣了但是未扣减库存，此时就出现了数据不一致现象，也就是出现了分布式事务问题，而要解决数据最终一致性很简单，只需要重新启动商品系统即可。

最后，重启商品系统，从系统日志中可见后台事务补偿线程消费事务消息重新发起扣减库存操作，商品表总库存字段值减1，事务补偿操作成功，分布式事务最终达成一致。

```
2021-11-23 14:09:30 [pool-2-thread-1] INFO  - consume record offset:1,key:null,value:Participant(transactionId=441573573, messageModel=2, destination=goods, targetClass=interface com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService, methodName=decrease, parameterTypes=[class com.lvyh.lightframe.transaction.dubbo.sample.goods.api.domain.InventoryRequest], arguments=[InventoryRequest(productId=1, count=1)])
2021-11-23 14:09:30 [pool-2-thread-1] INFO  - convert to transaction participant object, transactionId:441573573
2021-11-23 14:09:30 [pool-2-thread-1] INFO  - [TransactionInvocationServiceImpl] query transaction,transaction:null, transactionId:441573573
2021-11-23 14:09:30 [pool-2-thread-1] INFO  - [TransactionInvocationServiceImpl] transaction participants begin to invoke methods through reflection, transactionId:441573573
2021-11-23 14:09:30 [main] INFO  - Initializing ExecutorService 'applicationTaskExecutor'
2021-11-23 14:09:31 [pool-2-thread-1] INFO  - {dataSource-2} inited
2021-11-23 14:09:31 [pool-2-thread-1] INFO  - [AbstractTransactionAspect] Enter the interceptor!
2021-11-23 14:09:31 [pool-2-thread-1] INFO  - Enter the AOP method and obtain the RPC parameters transmitted from the remote as follows:null, the current local transaction role is:本地事务
2021-11-23 14:09:31 [pool-2-thread-1] INFO  - [LocalTransactionCoordinator] start to do proceed!
2021-11-23 14:09:31 [pool-2-thread-1] DEBUG - ==>  Preparing: select 'true' as QUERYID, id, product_id, total_inventory, create_time, update_time from transaction_inventory WHERE ( product_id = ? ) 
2021-11-23 14:09:31 [main] INFO  - Starting ProtocolHandler ["http-nio-9091"]
2021-11-23 14:09:31 [main] INFO  - Tomcat started on port(s): 9091 (http) with context path ''
2021-11-23 14:09:31 [main] INFO  - Started DubboSampleGoodsApplication in 8.811 seconds (JVM running for 9.577)
2021-11-23 14:09:31 [main] INFO  - [DubboSampleGoodsApplication] start up complete.
2021-11-23 14:09:31 [pool-2-thread-1] DEBUG - ==> Parameters: 1(Long)
2021-11-23 14:09:31 [pool-2-thread-1] DEBUG - <==      Total: 1
2021-11-23 14:09:31 [pool-2-thread-1] DEBUG - ==>  Preparing: update transaction_inventory SET product_id = ?, total_inventory = ?, create_time = ?, update_time = ? where id = ? 
2021-11-23 14:09:31 [pool-2-thread-1] DEBUG - ==> Parameters: 1(Long), 98(Integer), 2021-11-08 17:30:01.0(Timestamp), 2021-11-23 14:09:31.564(Timestamp), 1(Long)
2021-11-23 14:09:31 [pool-2-thread-1] DEBUG - <==    Updates: 1
2021-11-23 14:09:31 [pool-2-thread-1] INFO  - [AbstractTransactionAspect] Exit the interceptor!
2021-11-23 14:09:31 [pool-2-thread-1] INFO  - [TransactionInvocationServiceImpl] reflection invoke transaction success, and local transaction log status is set to commit, transactionId:441573573
2021-11-23 14:09:31 [pool-2-thread-1] INFO  - Transaction log insert, transaction:Transaction(transactionId=441573573, createTime=Tue Nov 23 14:09:30 CST 2021, lastTime=Tue Nov 23 14:09:30 CST 2021, role=2, status=3, targetClass=com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService, targetMethod=decrease, errorMessage=null, retriedCount=0, sendMessageCount=0, participants=[])
2021-11-23 14:09:31 [pool-2-thread-1] INFO  - [TransactionInvocationServiceImpl] regardless of success or failure, this transaction log needs to be inserted into the database, transactionId:441573573

```