/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.dubbo.sample.member.service;

import com.github.pagehelper.Page;
import com.lvyh.lightframe.transaction.common.util.BeanUtil;
import com.lvyh.lightframe.transaction.core.pagination.DefaultPageResult;
import com.lvyh.lightframe.transaction.core.pagination.PageParam;
import com.lvyh.lightframe.transaction.core.pagination.PageUtil;
import com.lvyh.lightframe.transaction.dubbo.sample.member.bean.TransactionUserAccountBean;
import com.lvyh.lightframe.transaction.dubbo.sample.member.dao.entity.TransactionUserAccountExample;
import com.lvyh.lightframe.transaction.dubbo.sample.member.dao.entity.TransactionUserAccount;
import com.lvyh.lightframe.transaction.dubbo.sample.member.dao.mapper.TransactionUserAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lvyh 2021/08/01.
 */
@Service
public class TransactionMemberMainDataService {
    private final static Logger logger = LoggerFactory.getLogger(TransactionMemberMainDataService.class);

    @Autowired
    private TransactionUserAccountMapper transactionUserAccountMapper;


    public boolean deleteTransactionUserAccount(Long id) throws Exception {
        if (id == null) {
            return false;
        }
        try {
            int updateNum = transactionUserAccountMapper.deleteByPrimaryKey(id);
            return updateNum == 1;
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public List<TransactionUserAccountBean> selectTransactionUserAccount(TransactionUserAccountBean condition) throws Exception {
        List<TransactionUserAccountBean> list = new ArrayList<TransactionUserAccountBean>();
        try {
            TransactionUserAccountExample example = new TransactionUserAccountExample();
            TransactionUserAccountExample.Criteria criteria = example.createCriteria();
            if (condition.getId() != null) {
                criteria.andIdEqualTo(condition.getId());
            }
            if (condition.getUserId() != null) {
                criteria.andUserIdEqualTo(condition.getUserId());
            }

            List<TransactionUserAccount> queryDataItems = transactionUserAccountMapper.selectByExample(example);
            if (CollectionUtils.isEmpty(queryDataItems)) {
                return list;
            }

            for (TransactionUserAccount transactionUserAccount : queryDataItems) {
                list.add(BeanUtil.toBean(TransactionUserAccountBean.class, BeanUtil.toMap(transactionUserAccount)));
            }

        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
        return list;
    }

    public DefaultPageResult<TransactionUserAccountBean> selectTransactionUserAccountByCondition(TransactionUserAccountBean condition, PageParam pageParam) throws Exception {
        if (pageParam == null) {
            throw new Exception("pageParam must need");
        }
        Page<TransactionUserAccount> page = PageUtil.startPage(pageParam);

        try {
            TransactionUserAccountExample example = new TransactionUserAccountExample();
            if (condition != null) {
                TransactionUserAccountExample.Criteria criteria = example.createCriteria();
                if (condition.getId() != null) {
                    criteria.andIdEqualTo(condition.getId());
                }

            }

            List<TransactionUserAccount> queryDataItems = transactionUserAccountMapper.selectByExample(example);
            DefaultPageResult<TransactionUserAccountBean> result = new DefaultPageResult<>();
            if (queryDataItems == null || queryDataItems.size() == 0) {
                return result;
            }

            List<TransactionUserAccountBean> list = new ArrayList<TransactionUserAccountBean>();
            for (TransactionUserAccount transactionUserAccount : queryDataItems) {
                list.add(BeanUtil.toBean(TransactionUserAccountBean.class, BeanUtil.toMap(transactionUserAccount)));
            }

            result.setResultList(list);
            result.setPageSize(page.getPageSize());
            result.setPageNo(page.getPageNum());
            result.setTotalCount((int) page.getTotal());
            result.setTotalPage(page.getPages());
            return result;
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public int countTransactionUserAccountByCondition(TransactionUserAccountBean condition) throws Exception {
        try {
            TransactionUserAccountExample example = new TransactionUserAccountExample();
            if (condition != null) {
                TransactionUserAccountExample.Criteria criteria = example.createCriteria();
                if (condition.getId() != null) {
                    criteria.andIdEqualTo(condition.getId());
                }


            }

            return transactionUserAccountMapper.countByExample(example);
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    @Transactional(rollbackFor = {RuntimeException.class})
    public boolean updateTransactionUserAccount(TransactionUserAccountBean transactionUserAccountBean) throws Exception {
        if (transactionUserAccountBean == null || transactionUserAccountBean.getId() <= 0) {
            return false;
        }
        try {
            transactionUserAccountBean.setUpdateTime(new Date());
            TransactionUserAccount updateBean = BeanUtil.toBean(TransactionUserAccount.class, BeanUtil.toMap(transactionUserAccountBean));
            int updateNum = transactionUserAccountMapper.updateByPrimaryKeySelective(updateBean);
            return updateNum == 1;
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public TransactionUserAccountBean selectTransactionUserAccountById(Long id) throws Exception {
        if (id <= 0) {
            return null;
        }
        try {
            TransactionUserAccount transactionUserAccount = transactionUserAccountMapper.selectByPrimaryKey(id);
            if (transactionUserAccount == null) {
                logger.warn("not found data, tid:{}", id);
                return null;
            }
            return BeanUtil.toBean(TransactionUserAccountBean.class, BeanUtil.toMap(transactionUserAccount));
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public Long saveTransactionUserAccount(TransactionUserAccountBean transactionUserAccountBean) throws Exception {
        if (transactionUserAccountBean == null) {
            return -1L;
        }
        try {
            transactionUserAccountBean.setCreateTime(new Date());
            transactionUserAccountBean.setUpdateTime(new Date());
            TransactionUserAccount insertBean = BeanUtil.toBean(TransactionUserAccount.class, BeanUtil.toMap(transactionUserAccountBean));
            transactionUserAccountMapper.insertSelective(insertBean);
            return insertBean.getId();
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

}
