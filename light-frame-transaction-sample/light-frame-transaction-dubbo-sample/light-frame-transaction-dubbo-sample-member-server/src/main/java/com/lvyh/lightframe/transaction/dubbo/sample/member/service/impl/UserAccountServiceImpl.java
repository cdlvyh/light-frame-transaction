/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.dubbo.sample.member.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.lvyh.lightframe.transaction.annotation.MyTransaction;
import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.common.enums.MessageModel;
import com.lvyh.lightframe.transaction.common.util.BeanUtil;
import com.lvyh.lightframe.transaction.dubbo.sample.member.api.domain.UserAccountDto;
import com.lvyh.lightframe.transaction.dubbo.sample.member.api.domain.UserAccountRequest;
import com.lvyh.lightframe.transaction.dubbo.sample.member.api.service.UserAccountService;
import com.lvyh.lightframe.transaction.dubbo.sample.member.bean.TransactionUserAccountBean;
import com.lvyh.lightframe.transaction.dubbo.sample.member.service.TransactionMemberMainDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@Service
@org.springframework.stereotype.Service
public class UserAccountServiceImpl implements UserAccountService {

    @Autowired
    private TransactionMemberMainDataService transactionMemberMainDataService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    @MyTransaction(destination = TransactionConstant.MQ_TOPIC_MEMBER, deliveryModel = MessageModel.TOPIC)
    public boolean payment(UserAccountRequest userAccountRequest) throws Exception {
        TransactionUserAccountBean transactionUserAccountBean = selectTransactionUserAccount(userAccountRequest.getUserId());
        if (transactionUserAccountBean.getBalance().compareTo(userAccountRequest.getTotalAmount()) <= 0) {
            throw new RuntimeException("资金不足！");
        }
        transactionUserAccountBean.setBalance(transactionUserAccountBean.getBalance() - userAccountRequest.getTotalAmount());
        transactionUserAccountBean.setUpdateTime(new Date());

        transactionMemberMainDataService.updateTransactionUserAccount(transactionUserAccountBean);
        return Boolean.TRUE;
    }

    public TransactionUserAccountBean selectTransactionUserAccount(long userId) throws Exception {
        TransactionUserAccountBean condition = new TransactionUserAccountBean();
        condition.setUserId(userId);
        List<TransactionUserAccountBean> result = transactionMemberMainDataService.selectTransactionUserAccount(condition);
        return !CollectionUtils.isEmpty(result) ? result.get(0) : null;
    }

    @Override
    public UserAccountDto getByUserId(Long userId) throws Exception {
        TransactionUserAccountBean condition = new TransactionUserAccountBean();
        condition.setUserId(userId);
        List<TransactionUserAccountBean> result = transactionMemberMainDataService.selectTransactionUserAccount(condition);
        return !CollectionUtils.isEmpty(result) ? BeanUtil.toBean(UserAccountDto.class, BeanUtil.toMap(result.get(0))) : null;
    }
}
