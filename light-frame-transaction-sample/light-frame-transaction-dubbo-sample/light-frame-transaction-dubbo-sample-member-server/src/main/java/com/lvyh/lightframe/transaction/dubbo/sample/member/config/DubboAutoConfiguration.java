/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.dubbo.sample.member.config;

import com.alibaba.dubbo.common.utils.NetUtils;
import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ProtocolConfig;
import com.alibaba.dubbo.config.ProviderConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.spring.AnnotationBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

@Configuration
public class DubboAutoConfiguration implements EnvironmentAware {
    private static final Logger logger = LoggerFactory.getLogger(DubboAutoConfiguration.class);
    private static final int DEFAULT_TIMEOUT = 5000;
    @Autowired
    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public static AnnotationBean annotationBean(@Value("com.lvyh.lightframe.transaction.dubbo.sample.member.service.impl") String packageName) {
        AnnotationBean annotationBean = new AnnotationBean();
        annotationBean.setPackage(packageName);
        return annotationBean;
    }

    @Bean
    public ProviderConfig providerConfig(ApplicationConfig applicationConfig, RegistryConfig registryConfig, ProtocolConfig protocolConfig) {
        ProviderConfig providerConfig = new ProviderConfig();
        providerConfig.setTimeout(Integer.parseInt(this.environment.getProperty("dubbo.provider.timeout", "15000")));
        providerConfig.setRetries(Integer.parseInt(this.environment.getProperty("dubbo.provider.retries", "1")));
        providerConfig.setDelay(Integer.parseInt(this.environment.getProperty("dubbo.provider.delay", "-1")));
        providerConfig.setApplication(applicationConfig);
        providerConfig.setRegistry(registryConfig);
        providerConfig.setProtocol(protocolConfig);
        return providerConfig;
    }

    @Bean
    public ProtocolConfig protocolConfig() {
        Integer port = NetUtils.getAvailablePort();
        logger.info("[DubboAutoConfiguration] random allocation protocol port:{}", port);
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setName(this.environment.getProperty("dubbo.protocol.name", "dubbo"));
        protocolConfig.setPort(port);
        protocolConfig.setThreads(200);
        return protocolConfig;
    }

    @Bean
    public ApplicationConfig buildApplicationConfig() {
        ApplicationConfig application = new ApplicationConfig();
        application.setName(getApplicationName());
        return application;
    }

    @Bean
    public RegistryConfig buildRegistryConfig() {
        RegistryConfig registry = new RegistryConfig();
        registry.setAddress(environment.getProperty("dubbo.registry.address"));
        registry.setFile(buildCacheFile());
        return registry;
    }

    private String buildCacheFile() {
        String cacheFile = System.getProperty("user.dir") + getApplicationName() + File.separator + "dubbo.cache";
        return cacheFile;
    }

    private String getApplicationName() {
        String applicationName = this.environment.getProperty("spring.application.name");
        return applicationName;
    }

}
