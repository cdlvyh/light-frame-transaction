/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.dubbo.sample.order.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.lvyh.lightframe.transaction.annotation.MyTransaction;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.api.domain.InventoryRequest;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService;
import com.lvyh.lightframe.transaction.dubbo.sample.member.api.domain.UserAccountRequest;
import com.lvyh.lightframe.transaction.dubbo.sample.member.api.service.UserAccountService;
import com.lvyh.lightframe.transaction.dubbo.sample.order.api.domain.TransactionOrderDto;
import com.lvyh.lightframe.transaction.dubbo.sample.order.api.service.OrderInfoService;
import com.lvyh.lightframe.transaction.dubbo.sample.order.bean.TransactionOrderBean;
import com.lvyh.lightframe.transaction.dubbo.sample.order.enums.OrderStatusEnum;
import com.lvyh.lightframe.transaction.dubbo.sample.order.service.TransactionOrderMainDataService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@Service
@org.springframework.stereotype.Service
@Slf4j
public class OrderInfoServiceImpl implements OrderInfoService {
    private static final Logger logger = LoggerFactory.getLogger(OrderInfoServiceImpl.class);

    @Autowired
    private TransactionOrderMainDataService transactionOrderMainDataService;
    @Reference(proxy = "customProxyFactory")
    private UserAccountService userAccountService;

    @Reference(proxy = "customProxyFactory")
    private InventoryService inventoryService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public TransactionOrderDto createOrder(Integer count, Long price) throws Exception {
        TransactionOrderBean orderInfo = new TransactionOrderBean();
        orderInfo.setBuyCount(count);
        orderInfo.setCreateTime(new Date());
        orderInfo.setProductId(1L);
        orderInfo.setUserId(1L);
        orderInfo.setTotalAmount(price * count);
        orderInfo.setOrderStatus(OrderStatusEnum.NOT_PAY.getCode());

        long id = transactionOrderMainDataService.saveTransactionOrder(orderInfo);

        logger.info("[OrderInfoServiceImpl] create transaction order success, orderId:{}", id);
        TransactionOrderDto transactionOrderDto = new TransactionOrderDto();
        BeanUtils.copyProperties(orderInfo, transactionOrderDto);
        transactionOrderDto.setId(id);
        return transactionOrderDto;
    }

    public void updateOrderStatus(TransactionOrderDto order) throws Exception {
        TransactionOrderBean transactionOrderBean = selectTransactionOrder(order.getId());
        if (transactionOrderBean == null) {
            return;
        }
        transactionOrderBean.setOrderStatus(order.getOrderStatus());
        transactionOrderMainDataService.updateTransactionOrder(transactionOrderBean);
    }

    public TransactionOrderBean selectTransactionOrder(long id) throws Exception {
        TransactionOrderBean condition = new TransactionOrderBean();
        condition.setId(id);
        List<TransactionOrderBean> result = transactionOrderMainDataService.selectTransactionOrder(condition);
        return !CollectionUtils.isEmpty(result) ? result.get(0) : null;
    }

    @Override
    @MyTransaction
    @Transactional(rollbackFor = Exception.class)
    public void makePayment(TransactionOrderDto transactionOrderDto) throws Exception {

        //Modify order status to successful
        transactionOrderDto.setOrderStatus(OrderStatusEnum.PAY_SUCCESS.getCode());
        updateOrderStatus(transactionOrderDto);
        logger.info("[OrderInfoServiceImpl] update transaction order status, orderId:{}", transactionOrderDto.getId());

        //Deduct user balance
        UserAccountRequest userAccountRequest = new UserAccountRequest();
        userAccountRequest.setTotalAmount(transactionOrderDto.getTotalAmount());
        userAccountRequest.setUserId(transactionOrderDto.getUserId());

        /**
         * Payment method of remote member system
         *
         * The call will be triggered:CustomInvokerInvocationHandler.invoke,
         * the transaction initiator passes the transaction context information to the downstream participant service
         */
        userAccountService.payment(userAccountRequest);
        logger.info("[OrderInfoServiceImpl] invoke member payment, orderId:{}", transactionOrderDto.getId());

        //Enter inventory deduction operation
        InventoryRequest inventoryRequest = new InventoryRequest();
        inventoryRequest.setCount(transactionOrderDto.getBuyCount());
        inventoryRequest.setProductId(transactionOrderDto.getProductId());

        /**
         * Inventory deduction method of remote goods system
         *
         * The call will be triggered:CustomInvokerInvocationHandler.invoke,
         * the transaction initiator passes the transaction context information to the downstream participant service
         */
        inventoryService.decrease(inventoryRequest);
        logger.info("[OrderInfoServiceImpl] invoke stock decrease, orderId:{}", transactionOrderDto.getId());

    }
}
