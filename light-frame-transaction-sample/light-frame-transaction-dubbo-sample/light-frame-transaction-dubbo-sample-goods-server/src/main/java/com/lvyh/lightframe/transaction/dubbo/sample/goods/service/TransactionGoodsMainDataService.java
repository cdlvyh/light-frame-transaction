/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.dubbo.sample.goods.service;

import com.github.pagehelper.Page;
import com.lvyh.lightframe.transaction.common.util.BeanUtil;
import com.lvyh.lightframe.transaction.core.pagination.DefaultPageResult;
import com.lvyh.lightframe.transaction.core.pagination.PageParam;
import com.lvyh.lightframe.transaction.core.pagination.PageUtil;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.bean.TransactionInventoryBean;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.dao.entity.TransactionInventory;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.dao.entity.TransactionInventoryExample;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.dao.mapper.TransactionInventoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lvyh 2021/08/01.
 */
@Service
public class TransactionGoodsMainDataService {
    private final static Logger logger = LoggerFactory.getLogger(TransactionGoodsMainDataService.class);

    @Autowired
    private TransactionInventoryMapper transactionInventoryMapper;


    public boolean deleteTransactionInventory(Long id) throws Exception {
        if (id == null) {
            return false;
        }
        try {
            int updateNum = transactionInventoryMapper.deleteByPrimaryKey(id);
            return updateNum == 1;
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public List<TransactionInventoryBean> selectTransactionInventory(TransactionInventoryBean condition) throws Exception {
        List<TransactionInventoryBean> list = new ArrayList<TransactionInventoryBean>();
        try {
            TransactionInventoryExample example = new TransactionInventoryExample();
            TransactionInventoryExample.Criteria criteria = example.createCriteria();
            if (condition.getId() != null) {
                criteria.andIdEqualTo(condition.getId());
            }
            if (condition.getProductId() != null) {
                criteria.andProductIdEqualTo(condition.getProductId());
            }

            List<TransactionInventory> queryDataItems = transactionInventoryMapper.selectByExample(example);
            if (CollectionUtils.isEmpty(queryDataItems)) {
                return list;
            }

            for (TransactionInventory transactionInventory : queryDataItems) {
                list.add(BeanUtil.toBean(TransactionInventoryBean.class, BeanUtil.toMap(transactionInventory)));
            }

        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
        return list;
    }

    public DefaultPageResult<TransactionInventoryBean> selectTransactionInventoryByCondition(TransactionInventoryBean condition, PageParam pageParam) throws Exception {
        if (pageParam == null) {
            throw new Exception("pageParam must need");
        }
        Page<TransactionInventory> page = PageUtil.startPage(pageParam);

        try {
            TransactionInventoryExample example = new TransactionInventoryExample();
            if (condition != null) {
                TransactionInventoryExample.Criteria criteria = example.createCriteria();
                if (condition.getId() != null) {
                    criteria.andIdEqualTo(condition.getId());
                }

            }

            List<TransactionInventory> queryDataItems = transactionInventoryMapper.selectByExample(example);
            DefaultPageResult<TransactionInventoryBean> result = new DefaultPageResult<>();
            if (queryDataItems == null || queryDataItems.size() == 0) {
                return result;
            }

            List<TransactionInventoryBean> list = new ArrayList<TransactionInventoryBean>();
            for (TransactionInventory transactionInventory : queryDataItems) {
                list.add(BeanUtil.toBean(TransactionInventoryBean.class, BeanUtil.toMap(transactionInventory)));
            }

            result.setResultList(list);
            result.setPageSize(page.getPageSize());
            result.setPageNo(page.getPageNum());
            result.setTotalCount((int) page.getTotal());
            result.setTotalPage(page.getPages());
            return result;
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public int countTransactionInventoryByCondition(TransactionInventoryBean condition) throws Exception {
        try {
            TransactionInventoryExample example = new TransactionInventoryExample();
            if (condition != null) {
                TransactionInventoryExample.Criteria criteria = example.createCriteria();
                if (condition.getId() != null) {
                    criteria.andIdEqualTo(condition.getId());
                }


            }

            return transactionInventoryMapper.countByExample(example);
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    @Transactional(rollbackFor = {RuntimeException.class})
    public boolean updateTransactionInventory(TransactionInventoryBean transactionInventoryBean) throws Exception {
        if (transactionInventoryBean == null || transactionInventoryBean.getId() <= 0) {
            return false;
        }
        try {
            transactionInventoryBean.setUpdateTime(new Date());
            TransactionInventory updateBean = BeanUtil.toBean(TransactionInventory.class, BeanUtil.toMap(transactionInventoryBean));
            int updateNum = transactionInventoryMapper.updateByPrimaryKeySelective(updateBean);
            return updateNum == 1;
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public TransactionInventoryBean selectTransactionInventoryById(Long id) throws Exception {
        if (id <= 0) {
            return null;
        }
        try {
            TransactionInventory transactionInventory = transactionInventoryMapper.selectByPrimaryKey(id);
            if (transactionInventory == null) {
                logger.warn("not found data, tid:{}", id);
                return null;
            }
            return BeanUtil.toBean(TransactionInventoryBean.class, BeanUtil.toMap(transactionInventory));
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public Long saveTransactionInventory(TransactionInventoryBean transactionInventoryBean) throws Exception {
        if (transactionInventoryBean == null) {
            return -1L;
        }
        try {
            transactionInventoryBean.setCreateTime(new Date());
            transactionInventoryBean.setUpdateTime(new Date());
            TransactionInventory insertBean = BeanUtil.toBean(TransactionInventory.class, BeanUtil.toMap(transactionInventoryBean));
            transactionInventoryMapper.insertSelective(insertBean);
            return insertBean.getId();
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }
}
