/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.dubbo.sample.goods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.lvyh.lightframe.transaction.annotation.MyTransaction;
import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.common.enums.MessageModel;
import com.lvyh.lightframe.transaction.common.util.BeanUtil;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.api.domain.InventoryDto;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.api.domain.InventoryRequest;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.api.service.InventoryService;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.service.TransactionGoodsMainDataService;
import com.lvyh.lightframe.transaction.dubbo.sample.goods.bean.TransactionInventoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@Service
@org.springframework.stereotype.Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private TransactionGoodsMainDataService transactionGoodsMainDataService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    @MyTransaction(destination = TransactionConstant.MQ_TOPIC_GOODS, deliveryModel = MessageModel.TOPIC)
    public boolean decrease(InventoryRequest inventoryRequest) throws Exception {
        TransactionInventoryBean transactionInventoryBean = selectTransactionInventory(inventoryRequest.getProductId());
        if (transactionInventoryBean.getTotalInventory() < inventoryRequest.getCount()) {
            throw new RuntimeException("库存不足");
        }
        transactionInventoryBean.setTotalInventory(transactionInventoryBean.getTotalInventory() - inventoryRequest.getCount());
        transactionInventoryBean.setUpdateTime(new Date());
        transactionGoodsMainDataService.updateTransactionInventory(transactionInventoryBean);
        return Boolean.TRUE;
    }

    public TransactionInventoryBean selectTransactionInventory(Long productId) throws Exception {
        TransactionInventoryBean condition = new TransactionInventoryBean();
        condition.setProductId(productId);
        List<TransactionInventoryBean> result = transactionGoodsMainDataService.selectTransactionInventory(condition);
        return !CollectionUtils.isEmpty(result) ? result.get(0) : null;
    }

    @Override
    public InventoryDto getByProductId(Long productId) throws Exception {
        TransactionInventoryBean condition = new TransactionInventoryBean();
        condition.setProductId(productId);
        List<TransactionInventoryBean> result = transactionGoodsMainDataService.selectTransactionInventory(condition);
        return !CollectionUtils.isEmpty(result) ? BeanUtil.toBean(InventoryDto.class, BeanUtil.toMap(result.get(0))) : null;
    }
}
