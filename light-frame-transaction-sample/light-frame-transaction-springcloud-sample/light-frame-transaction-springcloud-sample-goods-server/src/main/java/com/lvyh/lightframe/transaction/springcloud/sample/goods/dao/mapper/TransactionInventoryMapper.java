/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.sample.goods.dao.mapper;

import com.lvyh.lightframe.transaction.springcloud.sample.goods.dao.entity.TransactionInventory;
import com.lvyh.lightframe.transaction.springcloud.sample.goods.dao.entity.TransactionInventoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TransactionInventoryMapper {
    int countByExample(TransactionInventoryExample example);

    int deleteByExample(TransactionInventoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TransactionInventory record);

    int insertSelective(TransactionInventory record);

    List<TransactionInventory> selectByExample(TransactionInventoryExample example);

    TransactionInventory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TransactionInventory record, @Param("example") TransactionInventoryExample example);

    int updateByExample(@Param("record") TransactionInventory record, @Param("example") TransactionInventoryExample example);

    int updateByPrimaryKeySelective(TransactionInventory record);

    int updateByPrimaryKey(TransactionInventory record);
}