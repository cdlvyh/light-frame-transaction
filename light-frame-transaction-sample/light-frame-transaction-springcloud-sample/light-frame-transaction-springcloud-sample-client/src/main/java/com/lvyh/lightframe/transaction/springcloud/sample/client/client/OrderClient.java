/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.sample.client.client;

import com.lvyh.lightframe.transaction.annotation.MyTransaction;
import com.lvyh.lightframe.transaction.springcloud.sample.order.api.domain.TransactionOrderDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "light-frame-transaction-springcloud-sample-order-server")
public interface OrderClient {

    @MyTransaction
    @PostMapping("/order/makePayment")
    void makePayment(@RequestBody TransactionOrderDto transactionOrderDto) throws Exception;

    @RequestMapping("/order/createOrder")
    TransactionOrderDto createOrder(@RequestParam(value = "count") Integer count, @RequestParam(value = "amount") Long amount) throws Exception;

}
