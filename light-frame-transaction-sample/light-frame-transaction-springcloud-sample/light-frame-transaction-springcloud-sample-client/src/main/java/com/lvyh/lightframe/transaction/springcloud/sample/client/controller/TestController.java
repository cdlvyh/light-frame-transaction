/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.sample.client.controller;

import com.lvyh.lightframe.transaction.springcloud.sample.client.client.OrderClient;
import com.lvyh.lightframe.transaction.springcloud.sample.order.api.domain.TransactionOrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8080/create-order?count=1&price=1
 */
@RestController
public class TestController {

    @Autowired
    private OrderClient orderClient;

    @RequestMapping(value = "/create-order")
    public String createOrder(Integer count, Long price) throws Exception {

        TransactionOrderDto transactionOrderDto = orderClient.createOrder(count, price);
        orderClient.makePayment(transactionOrderDto);
        return "success";
    }
}
