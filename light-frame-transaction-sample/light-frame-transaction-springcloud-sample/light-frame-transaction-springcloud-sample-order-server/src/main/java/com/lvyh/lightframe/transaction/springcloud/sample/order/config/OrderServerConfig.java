/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.sample.order.config;

import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.common.config.message.KafkaMessageConfig;
import com.lvyh.lightframe.transaction.common.config.repository.JdbcRepositoryConfig;
import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.core.bootstrap.TransactionServerBootstrap;
import com.lvyh.lightframe.transaction.core.register.DynamicTransactionBeanRegister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(DynamicTransactionBeanRegister.class)
public class OrderServerConfig {
    private static final Logger logger = LoggerFactory.getLogger(OrderServerConfig.class);

    @Autowired
    private JdbcRepositoryConfig jdbcRepositoryConfig;

    @Autowired
    private KafkaMessageConfig kafkaMessageConfig;

    @Bean
    public TransactionServerBootstrap transactionServerBootstrap() {
        logger.info("[OrderServerConfig] init transaction server bootstrap.");
        TransactionServerBootstrap serverBootstrap = new TransactionServerBootstrap();
        TransactionApplicationConfig transactionApplicationConfig = new TransactionApplicationConfig();

        transactionApplicationConfig.setSerializer(TransactionConstant.SERIALIZE_JAVA);
        transactionApplicationConfig.setRepositoryName(TransactionConstant.TRANSACTION_REPOSITORY_JDBC);
        transactionApplicationConfig.setRepositoryConfig(jdbcRepositoryConfig);

        transactionApplicationConfig.setMessageQueueName(TransactionConstant.TRANSACTION_MESSAGE_KAFKA);
        transactionApplicationConfig.setMessageConfig(kafkaMessageConfig);

        serverBootstrap.setTransactionApplicationConfig(transactionApplicationConfig);
        return serverBootstrap;
    }
}
