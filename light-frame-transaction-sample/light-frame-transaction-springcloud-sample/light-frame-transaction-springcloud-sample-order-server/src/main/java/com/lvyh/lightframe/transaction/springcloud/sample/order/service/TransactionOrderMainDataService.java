/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.sample.order.service;

import com.github.pagehelper.Page;
import com.lvyh.lightframe.transaction.common.util.BeanUtil;
import com.lvyh.lightframe.transaction.core.pagination.DefaultPageResult;
import com.lvyh.lightframe.transaction.core.pagination.PageParam;
import com.lvyh.lightframe.transaction.core.pagination.PageUtil;
import com.lvyh.lightframe.transaction.springcloud.sample.order.bean.TransactionOrderBean;
import com.lvyh.lightframe.transaction.springcloud.sample.order.dao.entity.TransactionOrder;
import com.lvyh.lightframe.transaction.springcloud.sample.order.dao.entity.TransactionOrderExample;
import com.lvyh.lightframe.transaction.springcloud.sample.order.dao.mapper.TransactionOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lvyh 2021/08/01.
 */
@Service
public class TransactionOrderMainDataService {
    private final static Logger logger = LoggerFactory.getLogger(TransactionOrderMainDataService.class);

    @Autowired
    private TransactionOrderMapper transactionOrderMapper;


    public boolean deleteTransactionOrder(Long id) throws Exception {
        if (id == null) {
            return false;
        }
        try {
            int updateNum = transactionOrderMapper.deleteByPrimaryKey(id);
            return updateNum == 1;
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public List<TransactionOrderBean> selectTransactionOrder(TransactionOrderBean condition) throws Exception {
        List<TransactionOrderBean> list = new ArrayList<TransactionOrderBean>();
        try {
            TransactionOrderExample example = new TransactionOrderExample();
            TransactionOrderExample.Criteria criteria = example.createCriteria();
            if (condition.getId() != null) {
                criteria.andIdEqualTo(condition.getId());
            }


            List<TransactionOrder> queryDataItems = transactionOrderMapper.selectByExample(example);
            if (CollectionUtils.isEmpty(queryDataItems)) {
                return list;
            }

            for (TransactionOrder transactionOrder : queryDataItems) {
                list.add(BeanUtil.toBean(TransactionOrderBean.class, BeanUtil.toMap(transactionOrder)));
            }

        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
        return list;
    }

    public DefaultPageResult<TransactionOrderBean> selectTransactionOrderByCondition(TransactionOrderBean condition, PageParam pageParam) throws Exception {
        if (pageParam == null) {
            throw new Exception("pageParam must need");
        }
        Page<TransactionOrder> page = PageUtil.startPage(pageParam);

        try {
            TransactionOrderExample example = new TransactionOrderExample();
            if (condition != null) {
                TransactionOrderExample.Criteria criteria = example.createCriteria();
                if (condition.getId() != null) {
                    criteria.andIdEqualTo(condition.getId());
                }

            }

            List<TransactionOrder> queryDataItems = transactionOrderMapper.selectByExample(example);
            DefaultPageResult<TransactionOrderBean> result = new DefaultPageResult<>();
            if (queryDataItems == null || queryDataItems.size() == 0) {
                return result;
            }

            List<TransactionOrderBean> list = new ArrayList<TransactionOrderBean>();
            for (TransactionOrder transactionOrder : queryDataItems) {
                list.add(BeanUtil.toBean(TransactionOrderBean.class, BeanUtil.toMap(transactionOrder)));
            }

            result.setResultList(list);
            result.setPageSize(page.getPageSize());
            result.setPageNo(page.getPageNum());
            result.setTotalCount((int) page.getTotal());
            result.setTotalPage(page.getPages());
            return result;
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public int countTransactionOrderByCondition(TransactionOrderBean condition) throws Exception {
        try {
            TransactionOrderExample example = new TransactionOrderExample();
            if (condition != null) {
                TransactionOrderExample.Criteria criteria = example.createCriteria();
                if (condition.getId() != null) {
                    criteria.andIdEqualTo(condition.getId());
                }


            }

            return transactionOrderMapper.countByExample(example);
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    @Transactional(rollbackFor = {RuntimeException.class})
    public boolean updateTransactionOrder(TransactionOrderBean transactionOrderBean) throws Exception {
        if (transactionOrderBean == null || transactionOrderBean.getId() <= 0) {
            return false;
        }
        try {
            transactionOrderBean.setUpdateTime(new Date());
            TransactionOrder updateBean = BeanUtil.toBean(TransactionOrder.class, BeanUtil.toMap(transactionOrderBean));
            int updateNum = transactionOrderMapper.updateByPrimaryKeySelective(updateBean);
            return updateNum == 1;
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public TransactionOrderBean selectTransactionOrderById(Long id) throws Exception {
        if (id <= 0) {
            return null;
        }
        try {
            TransactionOrder transactionOrder = transactionOrderMapper.selectByPrimaryKey(id);
            if (transactionOrder == null) {
                logger.warn("not found data, tid:{}", id);
                return null;
            }
            return BeanUtil.toBean(TransactionOrderBean.class, BeanUtil.toMap(transactionOrder));
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public Long saveTransactionOrder(TransactionOrderBean transactionOrderBean) throws Exception {
        if (transactionOrderBean == null) {
            return -1L;
        }
        try {
            transactionOrderBean.setCreateTime(new Date());
            transactionOrderBean.setUpdateTime(new Date());
            TransactionOrder insertBean = BeanUtil.toBean(TransactionOrder.class, BeanUtil.toMap(transactionOrderBean));
            transactionOrderMapper.insertSelective(insertBean);
            return insertBean.getId();
        } catch (Exception exp) {
            logger.error(exp.getMessage(), exp);
            throw exp;
        }
    }

}
