/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.sample.order.client;

import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.common.enums.MessageModel;
import com.lvyh.lightframe.transaction.springcloud.annotation.TransactionFeignClient;
import com.lvyh.lightframe.transaction.springcloud.sample.member.api.domain.UserAccountDto;
import com.lvyh.lightframe.transaction.springcloud.sample.member.api.domain.UserAccountRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "light-frame-transaction-springcloud-sample-member-server")
public interface MemberService {

    @RequestMapping("/member/payment")
    @TransactionFeignClient(destination = TransactionConstant.MQ_TOPIC_MEMBER, deliveryModel = MessageModel.TOPIC, servicePath = "com.lvyh.lightframe.transaction.springcloud.sample.member.api.service.UserAccountService")
    boolean payment(@RequestBody UserAccountRequest userAccountRequest) throws Exception;

    @RequestMapping("/member/getByUserId")
    UserAccountDto getByUserId(@RequestParam("userId") Long userId) throws Exception;
}
