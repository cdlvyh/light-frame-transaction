/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.sample.member.controller;


import com.lvyh.lightframe.transaction.springcloud.sample.member.api.domain.UserAccountDto;
import com.lvyh.lightframe.transaction.springcloud.sample.member.api.domain.UserAccountRequest;
import com.lvyh.lightframe.transaction.springcloud.sample.member.api.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private UserAccountService userAccountService;

    @RequestMapping("/payment")
    public Boolean payment(@RequestBody UserAccountRequest userAccountRequest) throws Exception {
        return userAccountService.payment(userAccountRequest);
    }

    @RequestMapping("/getByUserId")
    public UserAccountDto getByUserId(@RequestParam("userId") Long userId) throws Exception {
        return userAccountService.getByUserId(userId);
    }


}
