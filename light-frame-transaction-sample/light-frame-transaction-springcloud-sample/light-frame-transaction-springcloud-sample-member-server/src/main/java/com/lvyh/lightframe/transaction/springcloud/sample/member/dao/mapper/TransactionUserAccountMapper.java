/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.springcloud.sample.member.dao.mapper;

import com.lvyh.lightframe.transaction.springcloud.sample.member.dao.entity.TransactionUserAccount;
import com.lvyh.lightframe.transaction.springcloud.sample.member.dao.entity.TransactionUserAccountExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TransactionUserAccountMapper {
    int countByExample(TransactionUserAccountExample example);

    int deleteByExample(TransactionUserAccountExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TransactionUserAccount record);

    int insertSelective(TransactionUserAccount record);

    List<TransactionUserAccount> selectByExample(TransactionUserAccountExample example);

    TransactionUserAccount selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TransactionUserAccount record, @Param("example") TransactionUserAccountExample example);

    int updateByExample(@Param("record") TransactionUserAccount record, @Param("example") TransactionUserAccountExample example);

    int updateByPrimaryKeySelective(TransactionUserAccount record);

    int updateByPrimaryKey(TransactionUserAccount record);
}