/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.kafka.config;

import com.lvyh.lightframe.transaction.common.config.message.KafkaMessageConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author lvyh 21/03/06.
 */
@Configuration
@EnableConfigurationProperties(KafkaMessageProperties.class)
public class KafkaMessageAutoConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(KafkaMessageAutoConfiguration.class);

    @Resource
    private KafkaMessageProperties kafkaMessageProperties;

    @Bean(value = "kafkaMessageConfig")
    @ConditionalOnProperty(prefix = "transaction.message.kafka", value = "enabled", havingValue = "true")
    public KafkaMessageConfig activemqMessageConfig() {
        logger.info("transaction message kafka config:{}", kafkaMessageProperties);
        KafkaMessageConfig kafkaMessageConfig = new KafkaMessageConfig();
        BeanUtils.copyProperties(kafkaMessageProperties, kafkaMessageConfig);
        return kafkaMessageConfig;
    }
}
