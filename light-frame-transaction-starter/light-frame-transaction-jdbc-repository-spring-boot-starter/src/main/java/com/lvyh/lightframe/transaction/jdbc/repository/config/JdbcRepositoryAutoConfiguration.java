/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.jdbc.repository.config;

import com.lvyh.lightframe.transaction.common.config.repository.JdbcRepositoryConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author lvyh 21/03/06.
 */
@Configuration
@EnableConfigurationProperties(JdbcRepositoryProperties.class)
public class JdbcRepositoryAutoConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(JdbcRepositoryAutoConfiguration.class);

    @Resource
    private JdbcRepositoryProperties jdbcRepositoryProperties;

    @Bean(value = "jdbcRepositoryConfig")
    @ConditionalOnProperty(prefix = "transaction.repository.jdbc", value = "enabled", havingValue = "true")
    public JdbcRepositoryConfig jdbcRepositoryConfig() {
        logger.info("transaction repository jdbc config:{}", jdbcRepositoryProperties);
        JdbcRepositoryConfig jdbcRepositoryConfig = new JdbcRepositoryConfig();
        BeanUtils.copyProperties(jdbcRepositoryProperties, jdbcRepositoryConfig);
        return jdbcRepositoryConfig;
    }
}
