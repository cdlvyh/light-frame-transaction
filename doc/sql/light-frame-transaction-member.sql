-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.18 - MySQL Community Server (GPL)
-- 服务器OS:                        Win64
-- HeidiSQL 版本:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for light-frame-transaction-member
CREATE DATABASE IF NOT EXISTS `light-frame-transaction-member` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `light-frame-transaction-member`;

-- Dumping structure for table light-frame-transaction-member.transaction_user_account
CREATE TABLE IF NOT EXISTS `transaction_user_account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `balance` int(11) DEFAULT NULL COMMENT '用户余额',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='交易用户账号表';

-- Dumping data for table light-frame-transaction-member.transaction_user_account: ~0 rows (大约)
/*!40000 ALTER TABLE `transaction_user_account` DISABLE KEYS */;
INSERT INTO `transaction_user_account` (`id`, `user_id`, `balance`, `create_time`, `update_time`) VALUES
	(1, 1, 100, '2021-11-08 17:30:01', '2021-11-23 11:19:48');
/*!40000 ALTER TABLE `transaction_user_account` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
