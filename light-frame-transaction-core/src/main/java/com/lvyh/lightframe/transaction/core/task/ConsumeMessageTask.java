/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.task;

import com.lvyh.lightframe.transaction.common.domain.Participant;
import com.lvyh.lightframe.transaction.core.service.TransactionInvocationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * @author lvyh 2021/06/12.
 */
@Slf4j
public class ConsumeMessageTask implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(ConsumeMessageTask.class);

    private KafkaConsumer<String, Participant> consumer;

    private TransactionInvocationService transactionInvocationService;

    private String topicName;

    public ConsumeMessageTask(KafkaConsumer<String, Participant> consumer, TransactionInvocationService transactionInvocationService, String topicName) {
        this.consumer = consumer;
        this.topicName = topicName;
        this.transactionInvocationService = transactionInvocationService;
    }

    @Override
    public void run() {
        logger.info("[ConsumeMessageTask] print topicName:{}", topicName);
        // A consumer can consume multiple topic at the same time
        consumer.subscribe(Arrays.asList(topicName));

        while (true) {
            ConsumerRecords<String, Participant> records = consumer.poll(1000);// Timeout
            for (ConsumerRecord<String, Participant> record : records) {
                try {
                    log.info("consume record offset:{},key:{},value:{}", record.offset(), record.key(), record.value());
                    Participant participant = record.value();
                    log.info("convert to transaction participant object, transactionId:{}", participant.getTransactionId());

                    //After getting the transaction participant from the message body, start preparing to call the method through reflection
                    transactionInvocationService.invoke(participant);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }


    }
}
