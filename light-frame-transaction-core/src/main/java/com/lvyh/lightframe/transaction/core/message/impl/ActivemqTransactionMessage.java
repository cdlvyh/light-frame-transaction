/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.message.impl;

import com.lvyh.lightframe.transaction.annotation.MyTransactionListener;
import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.common.domain.Participant;
import com.lvyh.lightframe.transaction.core.ext.Spi;
import com.lvyh.lightframe.transaction.core.message.TransactionMessage;
import com.lvyh.lightframe.transaction.core.serialize.Serializer;
import com.lvyh.lightframe.transaction.core.service.TransactionInvocationService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * ActiveMQ Message Queuing service
 */
@Slf4j
@Spi("activemqmessage")
public class ActivemqTransactionMessage implements TransactionMessage {

    private Serializer serializer;
    private TransactionInvocationService transactionInvocationService;

    @Override
    public String getMessageQueueName() {
        return TransactionConstant.TRANSACTION_MESSAGE_ACTIVEMQ;
    }

    @Override
    public void init(TransactionApplicationConfig transactionApplicationConfig) {
    }

    @Override
    public void setObjectSerializer(Serializer serializer) {
        this.serializer = serializer;
    }

    @Override
    public void setInvocationService(TransactionInvocationService transactionInvocationService) {
        this.transactionInvocationService = transactionInvocationService;
    }

    @Override
    public void send(Participant participant) {
    }

    @Override
    public void listen(List<MyTransactionListener> listeners) {

    }
}
