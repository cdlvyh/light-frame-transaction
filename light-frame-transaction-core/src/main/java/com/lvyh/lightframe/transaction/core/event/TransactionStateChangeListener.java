/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.event;

import com.google.common.eventbus.Subscribe;
import com.lvyh.lightframe.transaction.core.service.TransactionLogRepositoryService;
import com.lvyh.lightframe.transaction.core.util.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;

/**
 * @author lvyh 2021/05/15.
 */
@EventBusListener
@Slf4j
public class TransactionStateChangeListener {
    private TransactionLogRepositoryService transactionLogRepositoryService;

    @PostConstruct
    public void init() {
        transactionLogRepositoryService = SpringContextUtils.getBean(TransactionLogRepositoryService.class);
    }

    @Subscribe
    public void insert(TransactionInsertEvent event) throws InterruptedException {
        log.info("Transaction log insert, transaction:{}", event.getTransaction());
        transactionLogRepositoryService.save(event.getTransaction());
    }

    @Subscribe
    public void update(TransactionUpdateEvent event) throws InterruptedException {
        log.info("Transaction log update , transaction:{}", event.getTransaction());
        transactionLogRepositoryService.update(event.getTransaction());
    }
}
