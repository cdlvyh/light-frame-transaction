/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.coordinator;


import com.lvyh.lightframe.transaction.common.context.TransactionContext;
import com.lvyh.lightframe.transaction.common.enums.TransactionRole;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Transaction coordinator-provides local transaction role related processing
 */
@Component("LocalTransactionCoordinator")
public class LocalTransactionCoordinator implements RemoteTransactionCoordinator {
    private static final Logger logger = LoggerFactory.getLogger(LocalTransactionCoordinator.class);


    /**
     * The local transaction directly releases the pointcut and does not process the distributed transaction log,
     * otherwise the transaction log data will be inconsistent
     */
    public Object handler(ProceedingJoinPoint proceedingJoinPoint, TransactionContext remoteTransactionContext) throws Throwable {
        logger.info("[LocalTransactionCoordinator] start to do proceed!");
        return proceedingJoinPoint.proceed();
    }

    @Override
    public TransactionRole handlerRoleType() {
        return TransactionRole.LOCAL;
    }
}







