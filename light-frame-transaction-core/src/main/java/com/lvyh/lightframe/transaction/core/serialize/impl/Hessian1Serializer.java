/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.serialize.impl;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;
import com.lvyh.lightframe.transaction.common.exception.TransactionRuntimeException;
import com.lvyh.lightframe.transaction.core.ext.Spi;
import com.lvyh.lightframe.transaction.core.serialize.Serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Spi("hessian1")
public class Hessian1Serializer implements Serializer {

    private static Hessian1Serializer instance = new Hessian1Serializer();

    private Hessian1Serializer() {
    }

    public static Hessian1Serializer getInstance() {
        return instance;
    }

    @Override
    public byte[] serialize(final Object obj) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        HessianOutput ho = new HessianOutput(os);
        try {
            ho.writeObject(obj);
            ho.flush();
            byte[] result = os.toByteArray();
            return result;
        } catch (IOException e) {
            throw new TransactionRuntimeException(e);
        } finally {
            try {
                ho.close();
            } catch (IOException e) {
                throw new TransactionRuntimeException(e);
            }
            try {
                os.close();
            } catch (IOException e) {
                throw new TransactionRuntimeException(e);
            }
        }
    }

    @Override
    public <T> T deserialize(final byte[] bytes, final Class<T> clazz) {
        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        HessianInput hi = new HessianInput(is);
        try {
            Object result = hi.readObject();
            return (T) result;
        } catch (IOException e) {
            throw new TransactionRuntimeException(e);
        } finally {
            try {
                hi.close();
            } catch (Exception e) {
                throw new TransactionRuntimeException(e);
            }
            try {
                is.close();
            } catch (IOException e) {
                throw new TransactionRuntimeException(e);
            }
        }
    }

}
