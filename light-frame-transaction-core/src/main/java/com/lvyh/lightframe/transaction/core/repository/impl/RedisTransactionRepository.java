/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.repository.impl;


import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.common.constant.TransactionConstant;
import com.lvyh.lightframe.transaction.common.domain.Transaction;
import com.lvyh.lightframe.transaction.core.ext.Spi;
import com.lvyh.lightframe.transaction.core.repository.TransactionLogRepository;
import com.lvyh.lightframe.transaction.core.serialize.Serializer;

import java.util.Date;
import java.util.List;

@Spi("redisrepository")
public class RedisTransactionRepository implements TransactionLogRepository {

    @Override
    public void setObjectSerializer(Serializer objectSerializer) {

    }

    @Override
    public String getRepositoryName() {
        return TransactionConstant.TRANSACTION_REPOSITORY_REDIS;
    }

    @Override
    public void init(TransactionApplicationConfig rmdtConfig) {
        System.out.println();
    }

    @Override
    public Integer insert(Transaction transaction) {
        return 0;
    }

    @Override
    public Integer update(Transaction transaction) {
        return 0;
    }

    @Override
    public Transaction getById(String transactionId) {
        return null;
    }

    @Override
    public List<Transaction> findRecover(Date date, Integer retriedPeriod) {
        return null;
    }

}
