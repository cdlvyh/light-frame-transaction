/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.context;

import org.springframework.context.ConfigurableApplicationContext;

/**
 * Store spring application environment context objects
 */
public class ApplicationContextHolder {

    private static ConfigurableApplicationContext applicationContext;

    public static void setApplicationContext(final ConfigurableApplicationContext applicationContext) {
        ApplicationContextHolder.applicationContext = applicationContext;
    }

    public static <T> T getBean(String beanName) {
        return (T) applicationContext.getBean(beanName);
    }

    public static <T> T getBean(Class<T> clazz) {
        return (T) applicationContext.getBean(clazz);
    }

    public static void registerBean(String beanName, Object obj) {
        applicationContext.getBeanFactory().registerSingleton(beanName, obj);
    }

    public static ConfigurableApplicationContext getApplicationContext() {
        return applicationContext;
    }
}














