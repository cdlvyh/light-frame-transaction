/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.bootstrap;

import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.core.context.ApplicationContextHolder;
import com.lvyh.lightframe.transaction.core.event.TransactionEventProducer;
import com.lvyh.lightframe.transaction.core.ext.ExtensionLoaderFactory;
import com.lvyh.lightframe.transaction.core.message.TransactionMessage;
import com.lvyh.lightframe.transaction.core.repository.TransactionLogRepository;
import com.lvyh.lightframe.transaction.core.serialize.Serializer;
import com.lvyh.lightframe.transaction.core.service.TransactionInvocationService;
import com.lvyh.lightframe.transaction.core.service.TransactionLogRepositoryService;
import com.lvyh.lightframe.transaction.core.service.TransactionMessageService;
import com.lvyh.lightframe.transaction.core.service.TransactionRecoverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Initialize framework support classes
 */
@Component
public class TransactionServerBootstrapSupport {
    private static final Logger logger = LoggerFactory.getLogger(TransactionServerBootstrapSupport.class);

    /**
     * Transaction log event related services
     */
    @Autowired
    private TransactionEventProducer transactionEventProducer;

    /**
     * Transaction log data storage service
     */
    @Autowired
    private TransactionLogRepositoryService transactionLogRepositoryService;

    /**
     * Transaction log message queue service
     */
    @Autowired
    private TransactionMessageService transactionMessageService;

    /**
     * Method reflection invokes the service
     */
    @Autowired
    private TransactionInvocationService transactionInvocationService;

    /**
     * Error transaction recovery scheduled task service
     */
    @Autowired
    private TransactionRecoverService transactionRecoverService;

    public volatile static boolean executorStoped = false;

    /**
     * Initialization framework related information
     */
    public void initApplicationConfig(TransactionApplicationConfig transactionApplicationConfig) {

        //Support for loading data storage through SPI
        loadRepository(transactionApplicationConfig);
        logger.info("[TransactionServerBootstrapSupport] load data repository plugin through SPI.");

        //Load MQ support via SPI
        loadMessageQueue(transactionApplicationConfig);
        logger.info("[TransactionServerBootstrapSupport] load message queue plugin through SPI.");

        //Start the transaction log event handler
        transactionEventProducer.start();
        logger.info("[TransactionServerBootstrapSupport] start up transaction log event processing component.");

        // Start transaction log storage related support
        transactionLogRepositoryService.start(transactionApplicationConfig);
        logger.info("[TransactionServerBootstrapSupport] start up transaction log store component.");

        //Start transaction log message related support
        transactionMessageService.start(transactionApplicationConfig);
        logger.info("[TransactionServerBootstrapSupport] start up transaction log reliable message component.");

        //Regularly check the error transaction log, resend the MQ message call, and restore the transaction consistency
        transactionRecoverService.start(transactionApplicationConfig);
        logger.info("[TransactionServerBootstrapSupport] start up transaction recovery compensation component.");

    }

    private void loadRepository(TransactionApplicationConfig transactionApplicationConfig) {
        Serializer serializer = ExtensionLoaderFactory.getExtensionLoader(Serializer.class).getExtension(transactionApplicationConfig.getSerializer());
        TransactionLogRepository transactionRepository = ExtensionLoaderFactory.getExtensionLoader(TransactionLogRepository.class).getExtension(transactionApplicationConfig.getRepositoryName());
        transactionRepository.setObjectSerializer(serializer);
        ApplicationContextHolder.registerBean(TransactionLogRepository.class.getName(), transactionRepository);
    }

    private void loadMessageQueue(TransactionApplicationConfig transactionApplicationConfig) {
        Serializer serializer = ExtensionLoaderFactory.getExtensionLoader(Serializer.class).getExtension(transactionApplicationConfig.getSerializer());

        TransactionMessage transactionMessage = ExtensionLoaderFactory.getExtensionLoader(TransactionMessage.class).getExtension(transactionApplicationConfig.getMessageQueueName());
        transactionMessage.setObjectSerializer(serializer);

        //Set a method reflection calling service for the Message Queuing service provider
        transactionInvocationService.setTransactionConfig(transactionApplicationConfig);
        transactionMessage.setInvocationService(transactionInvocationService);
        ApplicationContextHolder.registerBean(TransactionMessage.class.getName(), transactionMessage);

    }

}
























