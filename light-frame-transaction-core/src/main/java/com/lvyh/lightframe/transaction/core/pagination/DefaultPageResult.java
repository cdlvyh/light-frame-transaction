/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.pagination;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lvyh 2021/04/06.
 */
public class DefaultPageResult<T> implements Serializable {

    private int pageNo;
    private int totalCount;
    private int pageSize;
    private Integer totalPage;
    private List<T> resultList;

    public void add(T obj) {
        if (resultList == null) {
            resultList = new ArrayList<T>();
        }
        resultList.add(obj);
    }

    public void addAll(List<T> objList) {
        if (resultList == null) {
            resultList = new ArrayList<T>();
        }
        resultList.addAll(objList);
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        if (totalPage == null) {
            int i = totalCount % pageSize;
            int j = totalCount / pageSize;
            totalPage = i == 0 ? j : j + 1;
        }
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getResultList() {
        return resultList;
    }

    public void setResultList(List<T> resultList) {
        this.resultList = resultList;
    }
}
