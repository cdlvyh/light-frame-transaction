/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.service;


import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.common.domain.Participant;

/**
 * Provides a way to call methods using reflection
 */
public interface TransactionInvocationService {


    /**
     * This method is called when the queue message consumer listens to a new message
     * The participant object is deserialized from the message body
     * A participant object represents a transaction participant
     * The information needed for the reflection call is contained in the participant object
     */
    void invoke(Participant participant);

    /**
     * When the transaction framework is initialized, it will inject the transaction configure object through this method
     *
     * @param transactionApplicationConfig
     */
    void setTransactionConfig(final TransactionApplicationConfig transactionApplicationConfig);
}
