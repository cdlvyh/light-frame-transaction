/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.event;

import com.lvyh.lightframe.transaction.common.domain.Transaction;
import com.lvyh.lightframe.transaction.core.util.SpringContextUtils;
import org.springframework.stereotype.Component;

/**
 * Transaction event producer
 */
@Component
public class TransactionEventProducer {

    private EventBusCenter eventBusCenter;

    /**
     * Start the transaction event handler
     */
    public void start() {
        eventBusCenter = SpringContextUtils.getBean(EventBusCenter.class);
    }

    /**
     * Publish transaction events
     *
     * @param transaction Transaction object
     * @param eventType   Transaction event type
     */
    public void publish(Transaction transaction, Integer eventType) {

        switch (eventType) {
            case 1:
                eventBusCenter.postSync(new TransactionInsertEvent(transaction));
                break;
            case 2:
                eventBusCenter.postSync(new TransactionUpdateEvent(transaction));
                break;
            default:
                break;
        }
    }

}
