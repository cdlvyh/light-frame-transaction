/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.coordinator;


import com.lvyh.lightframe.transaction.common.context.TransactionContext;
import com.lvyh.lightframe.transaction.common.enums.TransactionRole;
import com.lvyh.lightframe.transaction.common.thread.TransactionThreadLocal;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Transaction Coordinator - provides transaction initiator role related processing
 */
@Component("StartTransactionCoordinator")
public class StartTransactionCoordinator implements RemoteTransactionCoordinator {
    private static final Logger logger = LoggerFactory.getLogger(StartTransactionCoordinator.class);

    @Autowired
    private TransactionCoordinatorSupport transactionCoordinatorSupport;

    public Object handler(ProceedingJoinPoint proceedingJoinPoint, TransactionContext remoteTransactionContext) throws Throwable {
        try {
            logger.info("[StartTransactionCoordinator] begin transaction!");
            transactionCoordinatorSupport.begin(proceedingJoinPoint);

            //Execute the method of the pointcut. In the method, filter and intercept the called remote methods
            // and add transaction participants to the current transaction (transaction participants can also have transaction participants)
            Object proceed = proceedingJoinPoint.proceed();

            //Commit transaction
            transactionCoordinatorSupport.commit();
            logger.info("[StartTransactionCoordinator] commit transaction!");
            return proceed;
        } catch (Throwable throwable) {
            //If an error is thrown before or after the AOP pointcut, the current transaction is set as an error
            transactionCoordinatorSupport.fail(throwable);
            logger.info("[StartTransactionCoordinator] transaction error!");
            throw throwable;
        } finally {
            //MQ messages need to be sent whether there is an error or not
            logger.info("[StartTransactionCoordinator] send mq message to transaction participants!");
            transactionCoordinatorSupport.sendMessage();
            TransactionThreadLocal.clearThreadLocal();
        }
    }

    @Override
    public TransactionRole handlerRoleType() {
        return TransactionRole.START;
    }
}







