/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.service.impl;

import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.common.domain.Transaction;
import com.lvyh.lightframe.transaction.core.context.ApplicationContextHolder;
import com.lvyh.lightframe.transaction.core.repository.TransactionLogRepository;
import com.lvyh.lightframe.transaction.core.service.TransactionLogRepositoryService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TransactionLogRepositoryServiceImpl implements TransactionLogRepositoryService {

    private TransactionLogRepository transactionLogRepository;

    @Override
    public void start(TransactionApplicationConfig transactionApplicationConfig) {
        transactionLogRepository = ApplicationContextHolder.getBean(TransactionLogRepository.class.getName());
        transactionLogRepository.init(transactionApplicationConfig);
    }


    @Override
    public void save(Transaction transaction) {
        transactionLogRepository.insert(transaction);
    }


    @Override
    public void update(Transaction transaction) {
        transactionLogRepository.update(transaction);
    }

    @Override
    public Transaction getById(String transactionId) {
        return transactionLogRepository.getById(transactionId);
    }

    @Override
    public List<Transaction> findRecover(Date date, Integer retriedPeriod) {
        return transactionLogRepository.findRecover(date, retriedPeriod);
    }
}
