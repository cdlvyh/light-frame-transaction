/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.repository;


import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.common.domain.Transaction;
import com.lvyh.lightframe.transaction.core.ext.Extensible;
import com.lvyh.lightframe.transaction.core.serialize.Serializer;

import java.util.Date;
import java.util.List;

/**
 * Transaction log storage
 */
@Extensible
public interface TransactionLogRepository {

    void setObjectSerializer(Serializer serializer);

    /**
     * When loading the spi of the repository, determine which repository technology the client uses
     */
    String getRepositoryName();

    /**
     * Initialize repository related data, such as obtaining data sources, creating table structures, and so on.
     */
    void init(TransactionApplicationConfig transactionApplicationConfig);

    /**
     * Insert a transaction log data
     */
    Integer insert(Transaction transaction);

    /**
     * Update transaction log data
     */
    Integer update(Transaction transaction);

    /**
     * Get transaction object by transaction ID
     */
    Transaction getById(String transactionId);

    /**
     * Query the error transactions that need to be recovered
     */
    List<Transaction> findRecover(Date date, Integer retriedPeriod);
}
