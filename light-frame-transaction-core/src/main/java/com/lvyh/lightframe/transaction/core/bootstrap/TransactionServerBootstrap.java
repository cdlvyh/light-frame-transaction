/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.bootstrap;

import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.core.context.ApplicationContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * The distributed transaction framework starts the boot,
 * and the setApplicationContext method will be called after the spring container is initialized.
 * This object is created by the client user and configures the relevant information required for startup in TransactionApplicationConfig.
 */
public class TransactionServerBootstrap implements ApplicationContextAware {
    private static final Logger logger = LoggerFactory.getLogger(TransactionServerBootstrap.class);

    private TransactionApplicationConfig transactionApplicationConfig;

    @Autowired
    private TransactionServerBootstrapSupport transactionServerBootstrapSupport;

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        ApplicationContextHolder.setApplicationContext((ConfigurableApplicationContext) applicationContext);

        logger.info("[TransactionServerBootstrap] init transaction application config.......");
        transactionServerBootstrapSupport.initApplicationConfig(transactionApplicationConfig);
    }


    public TransactionApplicationConfig getTransactionApplicationConfig() {
        return transactionApplicationConfig;
    }

    public void setTransactionApplicationConfig(TransactionApplicationConfig transactionApplicationConfig) {
        this.transactionApplicationConfig = transactionApplicationConfig;
    }
}
