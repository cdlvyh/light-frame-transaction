/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.service;

import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.common.domain.Transaction;

import java.util.Date;
import java.util.List;

/**
 * Transaction log storage service
 */
public interface TransactionLogRepositoryService {

    /**
     * Start transaction log storage related support
     */
    void start(TransactionApplicationConfig rmdtConfig);

    /**
     * Save transaction
     */
    void save(Transaction transaction);


    /**
     * Modify transaction
     */
    void update(Transaction transaction);

    /**
     * Get transaction object by transaction ID
     */
    Transaction getById(String transactionId);

    /**
     * Query error transactions that need to be recovered
     */
    List<Transaction> findRecover(Date date, Integer retriedPeriod);
}
















