/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.message;

import com.lvyh.lightframe.transaction.annotation.MyTransactionListener;
import com.lvyh.lightframe.transaction.common.config.TransactionApplicationConfig;
import com.lvyh.lightframe.transaction.common.domain.Participant;
import com.lvyh.lightframe.transaction.core.ext.Extensible;
import com.lvyh.lightframe.transaction.core.serialize.Serializer;
import com.lvyh.lightframe.transaction.core.service.TransactionInvocationService;

import java.util.List;

/**
 * Send message service class
 */
@Extensible
//@ThreadSafe
public interface TransactionMessage {

    /**
     * Initialize MQ configuration
     */
    void init(TransactionApplicationConfig transactionApplicationConfig);

    /**
     * Set object serialization tool
     */
    void setObjectSerializer(Serializer serializer);

    /**
     * Set the service invoked by the method reflection
     */
    void setInvocationService(TransactionInvocationService transactionInvocationService);

    String getMessageQueueName();

    /**
     * Send messages to transaction participants
     * In extreme cases, the server of the transaction participant is down. After restarting the server,
     * consume the message and re execute the method to achieve transaction consistency
     */
    void send(Participant participant);

    /**
     * Message queue location to listen
     */
    void listen(List<MyTransactionListener> listeners);
}
