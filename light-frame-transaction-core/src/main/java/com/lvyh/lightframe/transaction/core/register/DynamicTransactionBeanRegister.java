/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.transaction.core.register;

import com.lvyh.lightframe.transaction.core.event.EventBusCenter;
import com.lvyh.lightframe.transaction.core.event.TransactionStateChangeListener;
import com.lvyh.lightframe.transaction.core.util.BeanRegistrationUtil;
import com.lvyh.lightframe.transaction.core.util.SpringContextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author lvyh 2021/05/15.
 */
public class DynamicTransactionBeanRegister implements ImportBeanDefinitionRegistrar, EnvironmentAware {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Configuration context is the tool to get configuration files
     */
    private Environment environment;

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        if (logger.isInfoEnabled()) {
            logger.info("register bean definitions.");
        }

        BeanRegistrationUtil.registerBeanDefinitionIfNotExists(registry, "springContextUtils", SpringContextUtils.class);
        BeanRegistrationUtil.registerBeanDefinitionIfNotExists(registry, "eventBusCenter", EventBusCenter.class);
        BeanRegistrationUtil.registerBeanDefinitionIfNotExists(registry, "transactionStateChangeListener", TransactionStateChangeListener.class);

    }

    @Override
    public void setEnvironment(Environment environment) {
        logger.info("start register bean.");
        this.environment = environment;
    }

}
