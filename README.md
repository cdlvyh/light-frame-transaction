## light-frame-transaction

![license](https://img.shields.io/badge/license-Apache--2.0-green.svg)

light-frame-transaction 是一款轻量级的分布式事务框架，旨在提供可靠消息 + 补偿重试方式解决系列分布式事务问题, 目前主要支持 dubbo、springcloud 等 rpc 框架分布式事务操作场景。

![架构设计](https://gitee.com/cdlvyh/light-frame-transaction/raw/master/doc/image/architecture_design.png)

## 一、功能简介

light-frame-transaction 是一款简洁明了的分布式事务框架，主要提供以下能力：

- RPC框架支持：支持 Dubbo、SpringCloud 等RPC框架
- 消息中间件支持：支持 Activimq、Kafka 等消息队列
- 事务日志存储支持：支持 Mysql、Redis 等存储方式
- 可靠消息服务：采用注解 + AOP 轻量级方案，实现基于可靠消息服务的分布式事务
- 事务补偿：支持事务补偿调度线程，服务故障或宕机仅需重新启动即可恢复事务
- 服务插件化：提供 Spi 及 Spring boot starter 插件机制，方便应用快速接入
- 高性能：去中心化设计，与业务系统完全融合，天然支持集群部署
- 使用简单：提供 Dubbo、SpringCloud 两种常见RPC框架分布式事务应用案例

## 二、快速开始

请查看项目文档中[快速开始](https://gitee.com/cdlvyh/light-frame-transaction/blob/master/quickstart.md)来了解如何快速上手使用 light-frame-transaction 。

## 三、如何贡献

light-frame-transaction 欢迎广大开发者创建Pull Request来贡献代码，代码通过审核后会被合并到master主分支。

light-frame-transaction 编译环境要求为JDK8或以上版本，需要采用 [Apache Maven 3.5.0](https://archive.apache.org/dist/maven/maven-3/3.5.0/binaries/) 或者更高的版本进行编译。

## 四、感谢

light-frame-transaction 部分借鉴参考了Hmily、Myth 和 RMDT 等分布式事务框架架构设计思路，在此表示感谢！

## 五、示例

light-frame-transaction 的示例工程，详见 light-frame-transaction-sample 。

## 六、开源许可

light-frame-transaction 基于Apache License 2.0协议，light-frame-transaction 依赖了一些第三方组件，其开源协议参见[依赖组件版权说明](https://gitee.com/cdlvyh/light-frame-transaction/blob/master/NOTICE.md)。
